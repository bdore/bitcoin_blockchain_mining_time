# A study on bitcoin block mining time difference

**[Check out the notebook](https://bitbucket.org/bdore/bitcoin_blockchain_mining_time/src/main/bitcoin_block_mining_time.ipynb?viewer=nbviewer)**.

## License
[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

This code is release under the BSD 3-Clause license.